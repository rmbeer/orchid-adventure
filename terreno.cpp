#include <stdio.h>
#include <stdlib.h>
#include <math.h>
#include "terreno.hpp"

Terreno::Terreno(){int i;
  tr=0;for(i=0;i<DEPTH;i++)Grad[i]=0;
}
Terreno::~Terreno(){int i;
  if(tr)free(tr);
  for(i=0;i<DEPTH;i++)free(Grad[i]);
}

void Terreno::GenGrad(){int zz,zzx,zzy,zx,zy;
  float a,b,f;
  //Prepara Gradientes
  for(zz=0;zz<DEPTH;zz++){zzx=(tx>>zz)+1;zzy=(ty>>zz)+1;
    //printf("T:%d - %d\n",zzx,zzy);
    for(zx=0;zx<zzx;zx++)for(zy=0;zy<zzy;zy++){
      a=((rand()%2000)-1000)/1000.0;
      b=((rand()%2000)-1000)/1000.0;
      f=sqrt(a*a+b*b);
      Grad[zz][(zx*zzx+zy)<<1]=a/f;
      Grad[zz][((zx*zzx+zy)<<1)+1]=b/f;
      //printf("%d,%d: %f\n",zx,zy,Grad[zz][(zx*zzx+zy)<<1]);
    }
  }
}

void Terreno::GenTr(){int zx,zy,i;float f;
  //Mapa
  GenGrad();
  for(zx=0;zx<tx;zx++)for(zy=0;zy<ty;zy++){
    f=perlin(zx,zy,DEPTH-1);
    if(f>.4){i=4;
    }else if(f>.01){i=2;
    }else if(f>-.1){i=1;
    }else{i=0;}
    tr[zx*tx+zy]=i;
  }//Arboles
  GenGrad();
  for(zx=0;zx<tx;zx++)for(zy=0;zy<ty;zy++){
    f=perlin(zx,zy,DEPTH-2);if(f>0)if(tr[zx*tx+zy]==2)tr[zx*tx+zy]=3;
  }//Montañas extras
  GenGrad();
  for(zx=0;zx<tx;zx++)for(zy=0;zy<ty;zy++){
    f=perlin(zx,zy,DEPTH-3);if(f>.5)if(tr[zx*tx+zy]>0)tr[zx*tx+zy]=4;
  }
}

int Terreno::NewTr(int ztx,int zty){int zx,zy;
  tx=ztx;ty=zty;tt=tx*ty;free(tr);
  for(zx=0;zx<DEPTH;zx++)free(Grad[zx]);
  //Limpia terreno
  Uchar*ztr=(Uchar*)malloc(tt);if(!ztr){printf("FALLO AAA\n");return -1;}
  tr=ztr;for(zx=0;zx<tx;zx++)for(zy=0;zy<ty;zy++)tr[zx*tx+zy]=0;
  //Reserva Gradientes
  for(zx=0;zx<DEPTH;zx++){
    Grad[zx]=(float*)malloc(((ty>>zx)+1)*((ty>>zx)+1)*sizeof(float)*2);
    if(!Grad[zx]){printf("FALLO %d\n",zx);return -1;}
  }
  return 0;
}

float Terreno::perlin(int x,int y,int zd){int zz,zz2=1;float f=0;
  zz=0;
  for(zz=zd;zz>=1;zz--){
    f+=Snoise(x,y,zz)/zz2;zz2<<=1;
  }
  return f;
  //f/=1.9375;return f;
}

float Terreno::Snoise(int x,int y,int oct){int zx,zy,zx2,zy2,zo1=1<<oct,zo3=zo1-1;
  float*a,*b,*c,*d,zo2=zo1,rx,ry,cx,cy;
  zx=x/zo1;zy=y/zo1;zx2=zx+1;zy2=zy+1;
  a=Grad[oct]+((zx*((tx>>oct)+1)+zy)<<1);
  b=Grad[oct]+((zx2*((tx>>oct)+1)+zy)<<1);
  c=Grad[oct]+((zx*((tx>>oct)+1)+zy2)<<1);
  d=Grad[oct]+((zx2*((tx>>oct)+1)+zy2)<<1);
  //printf("%d,%d: %f\n",x,y,a[0]);
  rx=(x&zo3)/(float)zo3;ry=(y&zo3)/(float)zo3;
  a[0]*rx+a[1]*ry;
  cx=rx*rx*(3.-2.*rx);
  cy=ry*ry*(3.-2.*ry);
  return lerp(lerp(a[0]*rx+a[1]*ry,b[0]*(rx-1.)+b[1]*ry,rx),
    lerp(c[0]*rx+c[1]*(ry-1.),d[0]*(rx-1.)+d[1]*(ry-1.),rx),ry);
}

// Function to linearly interpolate between a0 and a1
// Weight w should be in the range [0.0, 1.0]
float Terreno::lerp(float a0,float a1,float w){
  //return (1.0 - w)*a0 + w*a1;
  //return a0 + w*(a1 - a0);
  float f=w*w*(3-2*w);return a0*(1-f)+a1*f;
  //float f=6*pow(w,5)-15*pow(w,4)+10*pow(w,3);return a0*(1-f)+a1*f;
  //float f=-20*pow(w,7)+70*pow(w,6)-84*pow(w,5)+35*pow(w,4);return a0*(1-f)+a1*f;
}
