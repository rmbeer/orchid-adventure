#include <stdio.h>
#include "BaseBmp.h"
#include "terreno.hpp"

ALLEGRO_DISPLAY*dsp;
int CICLO,k;
ALLEGRO_BITMAP*scr,*fnd1,*pln1,*fnd2,*pln2,*gfnd;
ALLEGRO_BITMAP*gterreno,*gplayer,*gocean,*gfondos,*gnarciso,*gdirigible,*gfire;
ALLEGRO_BITMAP*gpln[4];
ALLEGRO_FONT*fnt;
ALLEGRO_TIMER*timer;
ALLEGRO_EVENT_QUEUE*evq;
int TAM_MAP=64;
int tfnt;

char*marq="Use facebook, use twitter, use instagram, use BETTER BEST BETTER PLUSH+ use telegram, wow use discord, use amazing amazon, use a redbull reddit GREAT COOL POWER MOTHERFUCKING SITWES YES YEEES OH YEEES FIREFOX I LOVE YOU TO DEATH TAKE MY SOUL BITCH x.X";

//============================================================================//
// FUNCIONES GENERALES                                                        //
//============================================================================//

ALLEGRO_COLOR pal[256];

void SetColorRawRGB(ALLEGRO_COLOR*zp,long zc){
  *zp=al_map_rgba((zc>>16)&255,(zc>>8)&255,zc&255,255);
}

void SetCPal(){
  long LC[16]={
    0x101010, //negro         0
    0xffffff, //blanco        1
    0xe04040, //rojo          2
    0x60ffff, //cian          3
    0xe060e0, //violeta       4
    0x40e040, //verde         5
    0x4040e0, //azul          6
    0xffff40, //amarillo      7
    0xe0a040, //naranja       8
    0x9c7448, //marron        9
    0xffa0a0, //rojo claro    a
    0x545454, //gris oscuro   b
    0x888888, //medio gris    c
    0xa0ffa0, //verde claro   d
    0xa0a0ff, //azul claro    e
    0xc0c0c0}; //gris claro    f
  for(int i=0;i<16;i++)
    SetColorRawRGB(pal+i,LC[i]);
}

int InitScreen(){int err=1;
  if(!al_init())return err;err++;
  if(!al_install_keyboard())return err;err++;
  if(!al_install_mouse())return err;err++;
  /* TODO: Revisar si para las 'VOICE' necesita ser unico para todas las ventanas */
  //if(!al_install_audio())ctrl&=~1;
  //if(ctrl&1){if(!al_init_acodec_addon())return;}err++;
  if(!al_init_image_addon())return err;err++;
  if(!al_init_primitives_addon())return err;err++;
  al_init_font_addon();
  if(!al_init_ttf_addon())return err;err++;
  evq=al_create_event_queue();
  if(!evq)return err;err++;
  timer=al_create_timer(1.0/60);
  if(!timer)return err;err++;
  al_register_event_source(evq,al_get_timer_event_source(timer));
  al_register_event_source(evq,al_get_keyboard_event_source());
  al_register_event_source(evq,al_get_mouse_event_source());
  al_set_new_display_flags(ALLEGRO_FRAMELESS);
  dsp=al_create_display(640,480);
  if(!dsp)return err;err++; //9
  al_register_event_source(evq,al_get_display_event_source(dsp));
  al_set_blender(ALLEGRO_ADD,ALLEGRO_ALPHA,ALLEGRO_INVERSE_ALPHA);
  scr=al_create_bitmap(320,240);
  if(!scr)return err;err++;
  gterreno=al_load_bitmap("./Datos/img/terreno.png");if(!gterreno)return err;err++;
  gplayer=al_load_bitmap("./Datos/img/Player.png");if(!gplayer)return err;err++;
  gnarciso=al_load_bitmap("./Datos/img/narciso.png");if(!gnarciso)return err;err++;
  gdirigible=al_load_bitmap("./Datos/img/dirigible.png");if(!gdirigible)return err;err++;
  gfire=al_load_bitmap("./Datos/img/fuego.png");if(!gfire)return err;err++;
  gfnd=al_load_bitmap("./Datos/img/cielo2.png");if(!gfnd)return err;err++;
  gfondos=al_load_bitmap("./Datos/img/fondos.png");if(!gfondos)return err;err++;
  gpln[0]=al_load_bitmap("./Datos/img/plnborde.png");if(!gpln[0])return err;err++;
  gpln[1]=al_load_bitmap("./Datos/img/plnliso.png");if(!gpln[1])return err;err++;
  fnt=al_load_ttf_font("./Datos/fnt/BubblegumSans-Regular.ttf",16,ALLEGRO_TTF_MONOCHROME);
  if(!fnt)return err;err++; //16
  tfnt=al_get_font_line_height(fnt);
  SetCPal();
  al_start_timer(timer);
  return 0;
}

void EndScreen(){
  al_uninstall_system();
}

/** Entrada de teclado */
void EvKey(ALLEGRO_EVENT&ev){
  switch(ev.type){
  case ALLEGRO_EVENT_KEY_DOWN:
    switch(ev.keyboard.keycode){
    case ALLEGRO_KEY_RIGHT:k|=1;break;
    case ALLEGRO_KEY_UP:k|=2;break;
    case ALLEGRO_KEY_LEFT:k|=4;break;
    case ALLEGRO_KEY_DOWN:k|=8;break;
    case ALLEGRO_KEY_Z:k|=16;break;
    case ALLEGRO_KEY_X:k|=32;break;
    case ALLEGRO_KEY_C:k|=64;break;
    case ALLEGRO_KEY_A:k|=128;break;
    case ALLEGRO_KEY_S:k|=256;break;
    case ALLEGRO_KEY_D:k|=512;break;
    case ALLEGRO_KEY_ENTER:k|=1024;break;
    case ALLEGRO_KEY_RSHIFT:k|=1048;break;
    case ALLEGRO_KEY_F5:k|=0x80000000;break;
    case ALLEGRO_KEY_ESCAPE:CICLO=0;break;
    //case ALLEGRO_KEY_Q:printf("PUTAO\n");break;
    }break;
  case ALLEGRO_EVENT_KEY_UP:
    switch(ev.keyboard.keycode){
    case ALLEGRO_KEY_RIGHT:k&=~1;break;
    case ALLEGRO_KEY_UP:k&=~2;break;
    case ALLEGRO_KEY_LEFT:k&=~4;break;
    case ALLEGRO_KEY_DOWN:k&=~8;break;
    case ALLEGRO_KEY_Z:k&=~16;break;
    case ALLEGRO_KEY_X:k&=~32;break;
    case ALLEGRO_KEY_C:k&=~64;break;
    case ALLEGRO_KEY_A:k&=~128;break;
    case ALLEGRO_KEY_S:k&=~256;break;
    case ALLEGRO_KEY_D:k&=~512;break;
    case ALLEGRO_KEY_ENTER:k&=~1024;break;
    case ALLEGRO_KEY_RSHIFT:k&=~1048;break;
    case ALLEGRO_KEY_F5:k&=~0x80000000;break;
    }break;
  //case ALLEGRO_EVENT_MOUSE_BUTTON_DOWN:
  //case ALLEGRO_EVENT_MOUSE_BUTTON_UP:
  }
}

//============================================================================//
// FUNCIONES JUEGO                                                            //
//============================================================================//

ALLEGRO_BITMAP*map=0;
int(*Ejec)()=0;
Terreno terr;
char*Acamh,*Acamv;
int tAcamh,tAcamv; //Caminos generados
int px,py,mpx,mpy,mbpx,mbpy; //Player: Pos., Map Pos., y bifur.Pos.
int ptim,pside; //Player tiempo y lado
int pwmapx=192,pwmapy=272;
int tbfx,tbfy; //Total de bifurcaciones
int tsmx,tsmy; //Total de seccion mapas
int ZoneCenter; //Zona Central de la bifurcacion
struct{
  int bx,by; //Base del recorte
  int sx,sy,sd; //Incremento y distancia
  int timani;
  int scroce,timscroce; //Scroll Oceano
}mnmap;
int*Lvl=0,tLvl; //Niveles y Total de niveles
struct{
  char tr[4]; //Terreno
  char ele[4]; //Elementos
  int sall,salr; //Salida izquierda/derecha (index map en bifurcacion)
  int dirl,dirr; //Direccion izquierda/derecha (RULD)
}lvl; //Nivel temporal
char*mapele; //Elementos en el Mapa (max TAM_MAP)
char*mapdir; //Direcciones en el Mapa (max tbfx/y , RULD)
/* Bifurcacion:
 * 0 - Nada
 * bit 7 - Zona Central
 * Camino:
 * 0 - Nada
 * 1 - 1 flor blanca
 * 2 - 2 flores blancas
 * 3 - 3 flores blancas
 */
int pscroll; //Variable auxiliar para scroll
struct{
  float x,y;
  int di;
}flw[6]; //Lista de Narcisos en pantalla
int tflw;

void GenerateCam(char*);

int GenerateMap(){int i,zx,zy;
  if(terr.NewTr(TAM_MAP,TAM_MAP))return -1;
  terr.GenTr();
  tbfx=(TAM_MAP/3)&~3;tbfy=(TAM_MAP/3)&~3;
  tsmx=tbfx>>2;tsmy=tbfy>>2;
  tAcamh=tbfx*(tbfy-1);
  tAcamv=(tbfx-1)*tbfy;
  Acamh=(char*)malloc(sizeof(char)*tAcamh);
  Acamv=(char*)malloc(sizeof(char)*tAcamv);
  mapele=(char*)malloc(sizeof(char)*TAM_MAP*TAM_MAP);
  mapdir=(char*)malloc(sizeof(char)*tbfx*tbfy);
  if(!Acamh||!Acamv||!mapele||!mapdir)return -2;
  for(zx=0;zx<tAcamh;zx++)Acamh[zx]=0; //Limpia caminos
  for(zx=0;zx<tAcamv;zx++)Acamv[zx]=0;
  for(zx=0;zx<TAM_MAP*TAM_MAP;zx++)mapele[zx]=0;
  for(zx=0;zx<tbfx*tbfy;zx++)mapdir[zx]=0;
  char Acam[24]; //24 int: Primero H luego V correlativo
  //Distribuye caminos
  {int zx,zy,ah,av,ax,ay;
    for(zx=0;zx<tsmx;zx++)for(zy=0;zy<tsmy;zy++){
      //printf("Sec.Map: %d - %d\n",zx,zy);
      GenerateCam(Acam);
      for(ax=0;ax<4;ax++)for(ay=0;ay<4;ay++){
        if(ax<3)Acamv[zx*tsmy*16+zy*4+ax*tsmy*4+ay]=Acam[12+ay+ax*4];
        if(ay<3)Acamh[zx*tsmy*16+zy*4+ax*tsmy*4+ay-ax-zx*4]=Acam[ay+ax*4-ax];
      }
    }
    //Reconexion de secciones
    for(zx=0;zx<tsmx;zx++)for(zy=0;zy<tsmy;zy++){
      ax=rand()%4;ay=rand()%4;
      if(zy<tsmy-1)Acamh[zx*tsmy*16-zx*4+zy*4+3+ax*tsmy*4-ax]=1;
      if(zx<tsmx-1)Acamv[zx*tsmy*16+tsmy*4*3+zy*4+ay]=1;
    }//Cuenta niveles a preparar
    tLvl=0;ah=0;
    for(zx=0;zx<tbfx;zx++)for(zy=0;zy<tbfy;zy++){
      if(zx<tbfx-1&&Acamv[zx*tbfx+zy])tLvl++;
      if(zy<tbfy-1&&Acamv[zx*tbfx+zy-zx])tLvl++;
    }//Prepara niveles y direcciones en bifurcaciones
    if(Lvl)free(Lvl);
    Lvl=(int*)malloc(sizeof(int)*tLvl*4);
    for(zx=0;zx<tbfx;zx++)for(zy=0;zy<tbfy;zy++){
      if(zx<tbfx-1&&Acamv[zx*tbfx+zy]){
        for(av=0;av<4;av++)Lvl[(ah<<2)+av]=(zx*3+1)*TAM_MAP+(zy*3+1+av);
        ah++;mapdir[zx*tbfx+zy]|=8;
      }
      if(zy<tbfy-1&&Acamh[zx*tbfx+zy-zx]){
        for(av=0;av<4;av++)Lvl[(ah<<2)+av]=(zx*3+1+av)*TAM_MAP+(zy*3+1);
        ah++;mapdir[zx*tbfx+zy]|=1;
      }
      if(zx>0&&Acamv[zx*tbfx+zy-tbfy])mapdir[zx*tbfx+zy]|=2;
      if(zy>0&&Acamh[zx*tbfx+zy-zx-1])mapdir[zx*tbfx+zy]|=4;
    }//Genera elementos
    for(zx=0;zx<tbfx;zx++)for(zy=0;zy<tbfy;zy++){
      if(zx<tbfx-1&&Acamv[zx*tbfx+zy]){
        mapele[zx*tbfx+zy+tbfy]=rand()%3+1;
        mapele[zx*tbfx+zy+tbfy*2]=rand()%3+1;
      }
      if(zy<tbfy-1&&Acamh[zx*tbfx+zy-zx]){
        mapele[zx*tbfx+zy+1]=rand()%3+1;
        mapele[zx*tbfx+zy+2]=rand()%3+1;
      }
    }
  }
  lvl.salr=mpx*TAM_MAP+mpy;
  lvl.sall=mpx*TAM_MAP+mpy;
  lvl.tr[0]=lvl.tr[1]=lvl.tr[2]=lvl.tr[3]=terr.tr[mpx*TAM_MAP+mpy];
  lvl.ele[0]=lvl.ele[1]=lvl.ele[2]=lvl.ele[3]=mapele[mpx*TAM_MAP+mpy];
  if(Acamv[mbpx*tbfy+mbpy-tbfy])lvl.dirl=1;else lvl.dirl=0;
  if(Acamh[mbpx*tbfy+mbpy-mbpx])lvl.dirr=0;else lvl.dirr=1;
  ZoneCenter=1;
  return 0;
}

void DrawDirigible(){
  //static*marq=al_create_bitmap();
  static int tmaxmarq=-al_get_text_width(fnt,marq),pmarq=150;
  static ALLEGRO_BITMAP*bmpmarq=al_create_bitmap(150,tfnt);
  static float dpx=-90,dpy=610;
  al_draw_bitmap(gdirigible,dpy,dpx,0);
  al_set_target_bitmap(bmpmarq);
  al_clear_to_color(pal[0]);
  //al_draw_filled_rectangle(70,45,220,45+tfnt,pal[0]);
  al_draw_text(fnt,pal[13],pmarq,0,0,marq);
  al_set_target_bitmap(scr);
  al_draw_bitmap(bmpmarq,dpy+40,dpx+15,0);
  al_draw_bitmap(gfire,dpy-4,dpx-30,0);
  al_draw_bitmap(gfire,dpy+30,dpx-40,0);
  al_draw_bitmap(gfire,dpy+70,dpx-45,0);
  al_draw_bitmap(gfire,dpy+190,dpx-40,0);
  al_draw_bitmap(gfire,dpy+200,dpx-30,0);
  dpy-=.25;dpx+=.05;
  pmarq--;if(pmarq<tmaxmarq)pmarq=150;
}

void _DebugCam1(char*cam){int zx,zy;
  for(zx=0;zx<3;zx++){
    for(zy=0;zy<3;zy++)printf(" %c",(cam[zy+zx*4-zx])?'#':'.');printf("\n");
    for(zy=0;zy<4;zy++)printf("%c ",(cam[12+zy+zx*4])?'#':'.');printf("\n");
  }for(zy=0;zy<3;zy++)printf(" %c",(cam[zy+zx*4-zx])?'#':'.');printf("\n");
}

//Genera caminos en una seccion del mapa
void GenerateCam(char*cam){
  int zx,zy,ah,av,aerr;char achk[16];
  //CAMINOS
  //Caminos Aleatorios
  for(zx=0;zx<24;zx++)cam[zx]=1;
  //TODO: Maximo puede eliminarse 9 caminos??
  zy=rand()%5+5;
  for(zx=0;zx<zy;zx++){ah=rand()%24;if(cam[ah])cam[ah]=0;else zx--;}
  //Comprueba si hay un fallo
  for(aerr=1;aerr;){aerr=0;//_DebugCam1(cam);
    //bifurcacion solitario
    for(zx=0;zx<4;zx++)for(zy=0;zy<4;zy++){
      av=12+zy+zx*4;ah=zy+zx*4-zx;
      if(!(zy==0||!cam[ah-1])&&(zy==3||!cam[ah])&&
        (zx==0||!cam[av-4])&&(zx==3||!cam[av]))aerr=1;
    }//Division
    for(zx=0;zx<16;zx++)achk[zx]=0;achk[0]=1;
    while(!aerr){aerr=2;
      for(zx=0;zx<4;zx++)for(zy=0;zy<4;zy++)if(!achk[(zx<<2)+zy]){
        av=12+zy+zx*4;ah=zy+zx*4-zx;
        if(zy>0&&cam[ah-1]&&achk[(zx<<2)+zy-1]){achk[(zx<<2)+zy]=1;aerr=0;}
        if(zy<3&&cam[ah]&&achk[(zx<<2)+zy+1]){achk[(zx<<2)+zy]=1;aerr=0;}
        if(zx>0&&cam[av-4]&&achk[(zx<<2)+zy-4]){achk[(zx<<2)+zy]=1;aerr=0;}
        if(zx<3&&cam[av]&&achk[(zx<<2)+zy+4]){achk[(zx<<2)+zy]=1;aerr=0;}
      }
    }if(aerr==2)aerr=0;
    for(zx=0;zx<16;zx++)if(achk[zx]==0)aerr=1;
    //Reacomoda camino
    if(aerr){
      //printf("FALLO!!\n");
      zx=rand()%24;zy=rand()%24;
      while(cam[zx]==0||zx==zy){zx++;if(zx>=24)zx=0;}
      while(cam[zy]==1||zx==zy){zy++;if(zy>=24)zy=0;}
      cam[zy]=1;cam[zx]=0;
    }
  }//printf("CORRECTO\n");
}

void DrawMap(){int zx,zy,spr,ax,ay,aerr,tsmx,tsmy;
  al_set_target_bitmap(map);
  for(zx=0;zx<TAM_MAP;zx++)for(zy=0;zy<TAM_MAP;zy++){
    spr=terr.tr[zx*TAM_MAP+zy];
    al_draw_bitmap_region(gterreno,(spr&15)<<4,spr&240,16,16,zy<<4,zx<<4,0);
  }
  //Caminos
  ax=(TAM_MAP/3)&~3;ay=(TAM_MAP/3)&~3;
  for(zx=0;zx<ax;zx++)for(zy=0;zy<ay;zy++){
    al_draw_filled_ellipse(zy*48+24,zx*48+30,8,2,pal[3]);
    if(zx<ax-1&&Acamv[zx*tbfx+zy]){
      al_draw_rectangle(zy*48+23.5,zx*48+32.5,zy*48+24.5,zx*48+76.5,pal[4],0);
    }
    if(zy<ay-1&&Acamh[zx*tbfx+zy-zx]){
      al_draw_rectangle(zy*48+32.5,zx*48+29.5,zy*48+63.5,zx*48+30.5,pal[4],0);
    }
    //al_draw_ellipse(zy*48+24,zx*48+30,8,2,pal[6],0);
  }
}

int EGame();
int EScroll();
void InitMap();
void DrawScene(ALLEGRO_BITMAP*,ALLEGRO_BITMAP*);
int IEGame(){
  if(!map)map=al_create_bitmap(TAM_MAP<<4,TAM_MAP<<4);
  al_stop_timer(timer);
  Ejec=EGame;px=98;py=152;ptim=0;pside=0;tflw=0;
  GenerateMap();
  DrawMap();
  al_set_target_bitmap(scr);
  al_clear_to_color(pal[0]);
  //Dibuja oceano
  gocean=al_create_bitmap(448,40);
  al_set_target_bitmap(gocean);
  al_clear_to_color(al_map_rgba(0,0,0,0));
  for(int i=0,j;i<56;i++){j=(i&8)?(i&7):(7-(i&7));j++;
    al_draw_filled_rectangle(i<<3,j,(i+1)<<3,40,al_map_rgba(64,64,255,192));
    al_draw_line((i<<3)+.5,j-.5,(i<<3)+8,j-.5,pal[1],0);
    al_draw_line((i<<3)+.5,j+1.5,(i<<3)+8,j+1.5,pal[1],0);
    al_draw_line((i<<3)+.5,j+2.5,(i<<3)+8,j+2.5,pal[1],0);
    al_draw_line((i<<3)+.5,j+4.5,(i<<3)+8,j+4.5,pal[1],0);
  }
  //Prepara fondos y planos
  fnd1=al_create_bitmap(320,192);if(!fnd1)return -1;
  fnd2=al_create_bitmap(320,192);if(!fnd2)return -1;
  pln1=al_create_bitmap(320,192);if(!pln1)return -1;
  pln2=al_create_bitmap(320,192);if(!pln2)return -1;
  InitMap();
  mnmap.timscroce=mnmap.scroce=0;
  al_resume_timer(timer);
  return 0;
}

void DrawScene(ALLEGRO_BITMAP*zfnd,ALLEGRO_BITMAP*zpln){
  //Dibuja fondos
  al_set_target_bitmap(zfnd);
  al_clear_to_color(al_map_rgba(0,0,0,0));
  al_draw_bitmap_region(gfondos,lvl.tr[0]<<7,0,128,192,-96,0,0);
  al_draw_bitmap_region(gfondos,lvl.tr[1]<<7,0,128,192,32,0,0);
  al_draw_bitmap_region(gfondos,lvl.tr[2]<<7,0,128,192,160,0,0);
  al_draw_bitmap_region(gfondos,lvl.tr[3]<<7,0,128,192,288,0,0);
  //Dibuja planos
  al_set_target_bitmap(zpln);
  al_clear_to_color(al_map_rgba(0,0,0,0));
  al_draw_bitmap_region(gpln[0],0,0,128,192,-96,0,0);
  al_draw_bitmap_region(gpln[1],0,0,128,192,32,0,0);
  al_draw_bitmap_region(gpln[1],0,0,128,192,160,0,0);
  al_draw_bitmap_region(gpln[0],0,0,128,192,288,0,0);
}

void DEBUG(){
  printf("mnmap:bx/y:%d,%d sx/y:%d,%d sd:%d\n",mnmap.bx,mnmap.by,mnmap.sx,mnmap.sy,mnmap.sd);
  printf("ply:mpx/y:%d,%d mbpx/y:%d,%d ZC:%d\n",mpx,mpy,mbpx,mbpy,ZoneCenter);
}

void InitMap(){
  printf("INITMAP\n");
  printf("tbfx/y:%d,%d tAcamh/v:%d,%d\n",tbfx,tbfy,tAcamh,tAcamv);
  mnmap.bx=(tbfx-1)*48;mnmap.by=0;mpx=tbfx*3-2;mpy=1;
  mbpx=tbfx-1;mbpy=0;
  lvl.tr[0]=lvl.tr[1]=lvl.tr[2]=lvl.tr[3]=terr.tr[mpx*tbfx+mpy];
  DrawScene(fnd1,pln1);
  DEBUG();
}

//Prepara el siguiente nivel y el scroll
void GoMap(int zlador){int i,j,zix,ziy,zdir,zsal,zinv,zbif;
  if(zlador){
    zdir=lvl.dirr;zsal=lvl.salr;zinv=0;
  }else{
    zdir=lvl.dirl;zsal=lvl.sall;zinv=1;
  }
  if(!ZoneCenter){
    switch(zdir){
    case 0:zix=0;ziy=1;break;
    case 1:zix=-1;ziy=0;break;
    case 2:zix=0;ziy=-1;break;
    case 3:zix=1;ziy=0;break;
    }zbif=mbpx*tbfy+mbpy;
  }
  if(!ZoneCenter&&(mapdir[zbif]==1||mapdir[zbif]==2||mapdir[zbif]==4||mapdir[zbif]==8)){
    //Crea nivel ZonaCenter
    for(j=0;j<4;j++){
      lvl.tr[(zinv)?3-j:j]=terr.tr[mpx*TAM_MAP+mpy];
      lvl.ele[(zinv)?3-j:j]=mapele[mpx*TAM_MAP+mpy];
    }
    lvl.sall=mpx*TAM_MAP+mpy;
    lvl.salr=mpx*TAM_MAP+mpy;
    mnmap.bx=(mpx-1)<<4;mnmap.by=(mpy-1)<<4;
    zbif=mbpx*tbfy+mbpy;
    switch(mapdir[zbif]){
    case 1:lvl.dirl=lvl.dirr=0;zix=0;ziy=1;break;
    case 2:lvl.dirl=lvl.dirr=1;zix=-1;ziy=0;break;
    case 4:lvl.dirl=lvl.dirr=2;zix=0;ziy=-1;break;
    case 8:lvl.dirl=lvl.dirr=3;zix=1;ziy=0;break;
    }ZoneCenter=1;
  }else{
    //Crea nivel normal
    switch(zdir){
    case 0:zix=0;ziy=1;break;
    case 1:zix=-1;ziy=0;break;
    case 2:zix=0;ziy=-1;break;
    case 3:zix=1;ziy=0;break;
    }
    for(i=0;i<tLvl;i++)if(
      (Lvl[(i<<2)]==mpx*TAM_MAP+mpy||Lvl[(i<<2)+3]==mpx*TAM_MAP+mpy)&&
      (Lvl[(i<<2)]==(mpx+zix*3)*TAM_MAP+mpy+ziy*3||
      Lvl[(i<<2)+3]==(mpx+zix*3)*TAM_MAP+mpy+ziy*3))break;
    for(j=0;j<4;j++){
      lvl.tr[(zinv)?3-j:j]=terr.tr[(mpx+zix*j)*TAM_MAP+mpy+ziy*j];
      lvl.ele[(zinv)?3-j:j]=mapele[(mpx+zix*j)*TAM_MAP+mpy+ziy*j];
    }
    if(zlador){
      lvl.sall=mpx*TAM_MAP+mpy;
      lvl.salr=(mpx+zix*3)*TAM_MAP+mpy+ziy*3;
      mnmap.bx=(mpx-1)<<4;mnmap.by=(mpy-1)<<4;
      mnmap.sx=zix;mnmap.sy=ziy;
      mbpx+=zix;mbpy+=ziy;mpx+=zix*3;mpy+=ziy*3;
    }else{
      lvl.salr=mpx*TAM_MAP+mpy;
      lvl.sall=(mpx+zix*3)*TAM_MAP+mpy+ziy*3;
      mnmap.bx=(mpx+zix*3-1)<<4;mnmap.by=(mpy+ziy*3-1)<<4;
      mnmap.sx=-zix;mnmap.sy=-ziy;
      mbpx-=zix;mbpy-=ziy;mpx-=zix*3;mpy-=ziy*3;
    }
    switch(zdir){
    case 0:lvl.dirl=2;lvl.dirr=0;break;
    case 1:lvl.dirl=3;lvl.dirr=1;break;
    case 2:lvl.dirl=0;lvl.dirr=2;break;
    case 3:lvl.dirl=1;lvl.dirr=3;break;
    }ZoneCenter=0;
  }DrawScene(fnd2,pln2);
  Ejec=EScroll;
  if(py==320)pscroll=320;else pscroll=-320;
  //Prepara flores
  tflw=0;
  printf("ELE:%d , %d\n",lvl.ele[1],lvl.ele[2]);
  for(i=0;i<lvl.ele[1];i++){
    flw[i].x=98;
    flw[i].y=(128/(lvl.ele[1]+1))*(i+1)+32;
  }tflw=lvl.ele[1];
  for(i=0;i<lvl.ele[2];i++){
    flw[tflw+i].x=98;
    flw[tflw+i].y=(128/(lvl.ele[2]+1))*(i+1)+160;
  }tflw+=lvl.ele[2];
  //DEBUG
  printf("GOMAP (%c)\n",(zlador)?'R':'L');DEBUG();
}

void DrawStatusBar(){
  //al_draw_rectangle(pwmapy+0.5,pwmapx+0.5,pwmapy+47.5,pwmapx+47.5,pal[1],0);
  if(ZoneCenter)
    al_draw_bitmap_region(map,mnmap.by,mnmap.bx,48,48,272,192,0);
  else
    al_draw_bitmap_region(map,mnmap.by+mnmap.sy*mnmap.sd,mnmap.bx+mnmap.sx*mnmap.sd,48,48,272,192,0);
  al_draw_bitmap_region(gplayer,48+(mnmap.timani&~15),0,16,16,288,208,0);
  al_draw_filled_rectangle(0,192,272,240,al_map_rgb(0,0,192));
}
void DrawGame(){
  //PANTALLA
  al_set_target_bitmap(scr);
  //al_clear_to_color(al_map_rgba(0,0,0,0));
  al_draw_bitmap(gfnd,0,0,0);
  /*
  al_draw_rectangle(0.5,0.5,319.5,pwmapx-0.5,pal[1],0);
  al_draw_rectangle(32.5,0.5,159.5,pwmapx-0.5,pal[1],0);
  al_draw_rectangle(160.5,0.5,287.5,pwmapx-0.5,pal[1],0);
  al_draw_rectangle(95.5,0.5,96.5,pwmapx-0.5,pal[2],0);
  al_draw_rectangle(223.5,0.5,224.5,pwmapx-0.5,pal[2],0);*/
  al_draw_bitmap(fnd1,0,0,0);
  //al_draw_filled_rectangle(0,130,320,138,pal[4]);
  al_draw_bitmap(gocean,mnmap.scroce,pwmapx-40,0);
  al_draw_bitmap(pln1,0,0,0);
  al_draw_bitmap_region(gplayer,0,0,16,32,py,px,pside);
  if(!pscroll)for(int i=0;i<tflw;i++)
    al_draw_bitmap_region(gnarciso,0,0,16,130-flw[i].x,flw[i].y,flw[i].x,0);
  DrawDirigible();
  DrawStatusBar();
  //RENDER
  al_set_target_backbuffer(dsp);
  al_draw_scaled_bitmap(scr,0,0,320,240,0,0,640,480,0);
  al_flip_display();
}

int aSeldir,aSelopt;

int ESelDir(){
  if(k&15){
    if(k&1)aSeldir=0;
    if(k&2)aSeldir=1;
    if(k&4)aSeldir=2;
    if(k&8)aSeldir=3;
    if(py==-16){lvl.dirl=aSeldir;GoMap(0);}
      else if(py==320){lvl.dirr=aSeldir;GoMap(1);}
  }
  mnmap.timani++;if(mnmap.timani>=32)mnmap.timani=0;
  mnmap.timscroce++;
  if(mnmap.timscroce>2){
    mnmap.scroce-=8;if(mnmap.scroce<=-128)mnmap.scroce=0;
    mnmap.timscroce=0;
  }
  DrawGame();
  // Flecha direccion
  if(aSelopt&1)al_draw_filled_triangle(307,211,307,221,312,216,pal[7]); //R
  if(aSelopt&2)al_draw_filled_triangle(291,205,301,205,296,200,pal[7]); //U
  if(aSelopt&4)al_draw_filled_triangle(285,211,285,221,280,216,pal[7]); //L
  if(aSelopt&8)al_draw_filled_triangle(291,227,301,227,296,232,pal[7]); //D
  return 0;
}

int EScroll(){
  if(pscroll<0){pscroll+=4;py+=4;}else{pscroll-=4;py-=4;}
  if(pscroll==0){Ejec=EGame;
    int za,zb,zc;al_get_blender(&za,&zb,&zc);
    al_set_blender(ALLEGRO_ADD,ALLEGRO_ONE,ALLEGRO_ZERO);
    al_set_target_bitmap(fnd1);al_draw_bitmap(fnd2,0,0,0);
    al_set_target_bitmap(pln1);al_draw_bitmap(pln2,0,0,0);
    al_set_blender(za,zb,zc);
  }
  mnmap.timani++;if(mnmap.timani>=32)mnmap.timani=0;
  mnmap.timscroce++;
  if(mnmap.timscroce>2){
    mnmap.scroce-=8;if(mnmap.scroce<=-128)mnmap.scroce=0;
    mnmap.timscroce=0;
  }
  //PANTALLA
  al_set_target_bitmap(scr);
  al_draw_bitmap(gfnd,0,0,0);
  al_draw_bitmap(fnd2,pscroll,0,0);
  if(pscroll<0)al_draw_bitmap(fnd1,pscroll+320,0,0);
    else al_draw_bitmap(fnd1,pscroll-320,0,0);
  al_draw_bitmap(gocean,mnmap.scroce,pwmapx-40,0);
  al_draw_bitmap(pln2,pscroll,0,0);
  if(pscroll<0)al_draw_bitmap(pln1,pscroll+320,0,0);
    else al_draw_bitmap(pln1,pscroll-320,0,0);
  al_draw_bitmap_region(gplayer,0,0,16,32,py,px,pside);
  DrawStatusBar();
  //RENDER
  al_set_target_backbuffer(dsp);
  al_draw_scaled_bitmap(scr,0,0,320,240,0,0,640,480,0);
  al_flip_display();
  return 0;
}

int EGame(){
  if(k&4&&py>-16){py-=2;pside=ALLEGRO_FLIP_HORIZONTAL;}
  if(k&1&&py<320){py+=2;pside=0;}
  //if(k&8&&px>240-(TAM_MAP<<4))px-=8;
  //if(k&2&&px<0)px+=8;
  if(py==-16||py==320){
    aSelopt=mapdir[mpx*tbfx+mpy];aSeldir=(py==320)?lvl.dirr:lvl.dirl;
    switch(aSeldir){
    case 0:aSelopt&=~4;break;
    case 1:aSelopt&=~8;break;
    case 2:aSelopt&=~1;break;
    case 3:aSelopt&=~2;break;
    }
    if(ZoneCenter==0&&aSelopt!=0&&aSelopt!=1&&aSelopt!=2&&aSelopt!=4&&aSelopt!=8)
      Ejec=ESelDir;
    else{
      if(py==-16)GoMap(0);else if(py==320)GoMap(1);
    }
  }
  mnmap.timani++;if(mnmap.timani>=32)mnmap.timani=0;
  mnmap.timscroce++;
  if(mnmap.timscroce>2){
    mnmap.scroce-=8;if(mnmap.scroce<=-128)mnmap.scroce=0;
    mnmap.timscroce=0;
  }
  if(!ZoneCenter)mnmap.sd=py*3/20;
  DrawGame();
  return 0;
}

//============================================================================//
// CICLO                                                                      //
//============================================================================//

int Bucle(){int VIS=0;
  ALLEGRO_EVENT ev;
  do{
    do{
      al_wait_for_event(evq,&ev);
      switch(ev.type){
      case ALLEGRO_EVENT_DISPLAY_CLOSE:CICLO=0;return 0;
      case ALLEGRO_EVENT_TIMER:VIS=1;break;
      case ALLEGRO_EVENT_DISPLAY_SWITCH_OUT:break;
      case ALLEGRO_EVENT_DISPLAY_SWITCH_IN:break;
      case ALLEGRO_EVENT_KEY_CHAR:
      case ALLEGRO_EVENT_KEY_UP:
      case ALLEGRO_EVENT_KEY_DOWN:
      case ALLEGRO_EVENT_MOUSE_AXES:
      case ALLEGRO_EVENT_MOUSE_BUTTON_UP:
      case ALLEGRO_EVENT_MOUSE_BUTTON_DOWN:
        EvKey(ev);break;
      }
    }while(!al_event_queue_is_empty(evq));
    if(VIS){
      if(Ejec())CICLO=0;
      VIS=0;
    }
  }while(CICLO);
}

int main(){int i;
  if(i=InitScreen()){printf("ERROR:%d\n",i);return -1;}
  Ejec=IEGame;CICLO=1;
  al_start_timer(timer);
  Bucle();
  EndScreen();
  return 0;
}
